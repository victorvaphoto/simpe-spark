import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;


public class SparkStreamingCSV {



    public static void main(String[] args) throws Exception {

        SparkSession spark = SparkSession
                .builder()
                .appName("Read From CSV")
                .getOrCreate();


        // only detects new files
        // Read all the csv files written atomically in a directory
        StructType userSchema = new StructType().add("name", "string").add("age", "integer");
        //notice this is untyped
        Dataset<Row> csvDF = spark
                .readStream()
                .option("sep", ";")
                .schema(userSchema)      // Specify schema of the csv files
                .csv("/Users/victorhuang/dev/spark/streamingSource");    // Equivalent to format("csv").load("/path/to/directory")


        StreamingQuery query = csvDF.writeStream()
                .outputMode("append")
                .format("console")
                .start();


        //append not available when aggregation with no water mark, bc aggregation updates and violate semantics
        //complete not available when no aggregation, perhaps otherwise output is too big

        //aggregation -> complete, update mode
        //no aggregation -> append, update

        query.awaitTermination();
    }
}
