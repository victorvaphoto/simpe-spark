
import org.apache.commons.net.ntp.TimeStamp;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;
import scala.Tuple2;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class StreamingWordCountWindowed {

    public static void main(String[] args) throws Exception {


        String host = "localhost";
        int port = 9999;
        int windowSize = 10; //10 seconds
        int slideSize = 5; //5 seconds //if slide size smaller than window size, than the resulting windows overlap
        if (slideSize > windowSize) {
            System.err.println("<slide duration> must be less than or equal to <window duration>");
        }
        String windowDuration = windowSize + " seconds";
        String slideDuration = slideSize + " seconds";

        SparkSession spark = SparkSession
                .builder()
                .appName("StreamingWordCountWindowed")
                .getOrCreate();

        // Create DataFrame representing the stream of input lines from connection to host:port
        Dataset<Row> lines = spark
                .readStream()
                .format("socket")
                .option("host", host)
                .option("port", port)
                .option("includeTimestamp", true)
                .load();

        Dataset<Tuple2<String, Timestamp>> asTuple = lines.as(Encoders.tuple(Encoders.STRING(), Encoders.TIMESTAMP())); //transform to tuple

        //Variation One on flat Map, work with tuple in transformation, or could have just go from row to row
        /*Dataset<Tuple2<String, Timestamp>> splitted = asTuple.flatMap((FlatMapFunction<Tuple2<String, Timestamp>, Tuple2<String, Timestamp>>) t ->
                Arrays.asList(t._1.split(" ")).stream().map(val -> new Tuple2<String, Timestamp>(val, t._2)).collect(Collectors.toList()).iterator(),
                Encoders.tuple(Encoders.STRING(), Encoders.TIMESTAMP()));*/


        //Variation Two on flat map another variation, provide type on lambda expression and no need to cast function int type
     /*   Dataset<Tuple2<String, Timestamp>> splitted = asTuple.flatMap( (Tuple2<String, Timestamp> t) ->
                Arrays.asList(t._1.split(" ")).stream().map(val -> new Tuple2<String, Timestamp>(val, t._2)).collect(Collectors.toList()).iterator(),
                Encoders.tuple(Encoders.STRING(), Encoders.TIMESTAMP()));*/

        //Variation Three on flat mapm, yet another variation, but no streaming
        Dataset<Tuple2<String, Timestamp>> splitted = asTuple.flatMap((Tuple2<String, Timestamp> t) ->
                {
                    List<Tuple2<String, Timestamp>> result = new ArrayList();
                    for (String word : t._1.split(" ")) {
                        result.add(new Tuple2<String, Timestamp>(word, t._2));
                    }

                    return result.iterator();

                },
                Encoders.tuple(Encoders.STRING(), Encoders.TIMESTAMP()));

        Dataset<Row> df = splitted.toDF("word", "timestamp"); //this also does column renaming


        //splitted.groupBy(functions.window(splitted.col("timestamp")))


        Dataset<Row> groupedCount = df.groupBy(
                functions.window(df.col("timestamp"), windowDuration, slideDuration),
                df.col("word")
                ).count().orderBy("window");


        // Start running the query that prints the windowed word counts to the console
        StreamingQuery query = groupedCount.writeStream()
                .outputMode("complete")
                .format("console")
                .option("truncate", "false")
                .start();

        query.awaitTermination();
    }
}