/* SimpleApp.java */

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.types.StructType;

public class DataFrameTyped {

    static public class DeviceData{
        private String device;
        private String deviceType;
        private Double signal;

        public DeviceData() {

        }

        public DeviceData(String device, String deviceType, Double signal) {
            this.device = device;
            this.deviceType = deviceType;
            this.signal = signal;
        }

        public String getDevice() {
            return device;
        }

        public void setDevice(String device) {
            this.device = device;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public Double getSignal() {
            return signal;
        }

        public void setSignal(Double signal) {
            this.signal = signal;
        }
    }

    public static void main(String[] args) {

        String deviceFile = "src/resources/deviceData.csv"; // Should be some file on your system
        SparkSession spark = SparkSession.builder().appName("Simple Application").getOrCreate();

        StructType deviceSchema = new StructType().add("device", "string").add("deviceType", "string").add("signal", "double");

        //without the schema the signal will be string by default
        Dataset<Row> deviceData = spark.read().schema(deviceSchema).option("header", true).csv(deviceFile).cache();


        deviceData.printSchema();

        //exameple untyped opt
        deviceData.filter("signal > 20").show();


        //typed dataset
        Dataset ds = deviceData.as(ExpressionEncoder.javaBean(DeviceData.class));
        ds.filter((FilterFunction<DeviceData>) val -> val.signal < 20).show();

        spark.stop();
    }
}
